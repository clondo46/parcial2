package co.edu.eafit.st0257.s20181;

public class InputBuffer {
   private Card[] input;
   private int entra = 0;
   private int sale = 0;
   private int contador = 0;
   
   public InputBuffer(int n) {
      input = new Card[n];
   }

   public synchronized void deployCard(Card card) {
       while(contador==input.length){
            try {
                wait();
            } catch(InterruptedException ie) { } 
       }
       contador++;
       input[entra] = card;
       //System.out.println("Ingresando " + card.toString());
       entra=(entra+1)%input.length;
       notifyAll();
       
   }

   public synchronized Card fetchCard() {
       while(contador==0){
            try {
                wait();
            } catch(InterruptedException ie) { } 
       }
       contador--;
       Card card = input[sale];
       //System.out.println("Saliendo " + card.toString());
       sale=(sale+1)%input.length;
       notifyAll();
      return card;
   }
}
