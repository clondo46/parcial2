package co.edu.eafit.st0257.s20181;

public class OutputBuffer {
   private Line output[];
   private int entra = 0;
   private int sale = 0;
   private int contador = 0;

   public OutputBuffer(int n) {
      output = new Line[n];
   }

   public synchronized void deployLine(Line line) {
    while(contador==output.length){
         try {
             wait();
         } catch(InterruptedException ie) { } 
    }
    contador++;
    output[entra] = line;
    entra=(entra+1)%output.length;
    //System.out.println("Ingresando " + line.toString());
    notifyAll();
}

   public synchronized Line fetchLine() {
    while(contador==0){
         try {
             wait();
         } catch(InterruptedException ie) { } 
    }
    contador--;
    Line line = output[sale];
    //System.out.println("Saliendo " + line.toString());
    sale=(sale+1)%output.length;
    notifyAll();
   return line;
}
}
