package co.eafit.dis.st0257.s20181.bridge;

public class Bridge {
   private int nCarsOnBridge = 0;
   private int nWestCars = 0;
   public Bridge() {
      this.nCarsOnBridge = 0;
   }

   private static void printCar(String place, int nCar) {
      System.out.println("nCar: " + nCar + place);
   }

   public synchronized void enterWest(int nCar) {
      nWestCars++;
      while(nCarsOnBridge!=0){
        try {
            wait();
        } catch(InterruptedException ie) { } 
      }
      printCar(" is entering by west", nCar);
      nCarsOnBridge++;
   }

   public synchronized void enterEast(int nCar) {
      while(nCarsOnBridge!=0 || nWestCars!=0){
        try {
            wait();
        } catch(InterruptedException ie) { } 
      }
      printCar(" is entering by east", nCar);
      nCarsOnBridge++;
   }
   
   public synchronized void leaveWest(int nCar) {      
      printCar(" is leaving by west", nCar);
      nCarsOnBridge--;
      notifyAll();  
   }

   public synchronized void leaveEast(int nCar) {
      printCar(" is leaving by east", nCar);
      nWestCars--;
      nCarsOnBridge--;
      notifyAll();
   }

   public int getNCarsOnBridge() {
      return nCarsOnBridge;
   }
}
